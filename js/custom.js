	/*active*/
	$('.panel').on('click', function(){
	  $(this).addClass('active').siblings().removeClass('active');
	});
	/*show*/
	$(document).ready(function(){
		$('.panel').click(function() {
	 		$('.check-ico').show();
		});
		/*tooltip*/
		$('#example').tooltip('show');
	});


	/*Input File*/
	$("input:file").change(function (){
		var fileName = $(this).val();
		if(fileName.length >0){
			$(this).parent().children('span').html(fileName);
		}
		else{
			$(this).parent().children('span').html("Choose file");
		}
	});
	/* file input preview*/
	function readURL(input) {
		if (input.files && input.files[0]) {
		var reader = new FileReader();            
			reader.onload = function (e) {
				$('.logoContainer img').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
		}
	}
	$("input:file").change(function(){
		readURL(this);
	});

	/*Select Box*/
	$('.customSelect select').each(function () {
		var title = $(this).children('option:first-child').html();
		$(this)
			.css({'z-index':10, 'opacity':0, '-khtml-appearance':'none'})
			.after('<span class="selText placeholderTxt">' + title + '</span><i class="dropArrow" ></i>')
			.change (function () {
				var selTxt = $('option:selected', this).text();
				$(this).next().text(selTxt);
				if (title != selTxt) {
					$(this).next().removeClass('placeholderTxt');
				} else {
					$(this).next().addClass('placeholderTxt');
				}
			})
	});

    $('.customSelect select').focus(function () {
        $(this).keyup(function () {
            var title = $(this).children('option:first-child').html();
            var selTxt = $('option:selected', this).text();
          	$(this).next().text(selTxt);
             	if (title != selTxt) {
                 	$(this).next().removeClass('placeholderTxt');
             	} else {
                 	$(this).next().text(selTxt);
             	}
        });
    });