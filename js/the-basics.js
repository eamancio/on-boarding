$(document).ready(function(){
var substringMatcher = function(strs) {
  return function findMatches(q, cb) {
    var matches, substrRegex;
    matches = [];
    substrRegex = new RegExp(q, 'i');
    $.each(strs, function(i, str) {
      if (substrRegex.test(str)) {
        matches.push({ value: str });
      }
    });
    cb(matches);
  };
};

$('#scrollable-dropdown-menu .typeahead').typeahead({
  hint: true,
  highlight: true,
  minLength: 3
},
  {
    name: 'employers',
    displayKey: 'value',
    source: substringMatcher(employers)
  });

$('#university-list .typeahead').typeahead({
  hint: true,
  highlight: true,
  minLength: 3
},
  {
    name: 'universities',
    displayKey: 'value',
    source: substringMatcher(universities)
  });

});