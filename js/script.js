(function($) {
	$(document).ready(function() {

    $('.autonumeric').autoNumeric('init');
    $("#date").inputmask("99/99/9999");
    $("#phone").inputmask("(9999) 999-9999");
    $("#tin").inputmask("99-9999999");
    $("#ssn").inputmask("999-99-9999");
  });


})(window.jQuery);